package com.assessment.docker.constant;

public final class ConstantClass {
	private ConstantClass() {
		throw new IllegalStateException("ConstantClass");
	}

	public static final String URI_ASSESSMENT_CONTROLLER = "/assessmentcontroller";
	public static final String URI_FIBONACCI = "/fibonacci";
	public static final String LENGTH = "length";
	public static final String URI_FACTORIAL = "/factorial";
	public static final String NUMBER = "number";
	
}
