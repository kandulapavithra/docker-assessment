package com.assessment.docker.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.assessment.docker.constant.ConstantClass;
import com.assessment.docker.service.AssessmentService;

@RestController
@RequestMapping(ConstantClass.URI_ASSESSMENT_CONTROLLER)
public class AssessmentControllerClass {

	@Autowired
	private AssessmentService assessmentService;

	@GetMapping(value = ConstantClass.URI_FIBONACCI)
	public List<Integer> getFibonacciMethod(
			@RequestParam(value = ConstantClass.LENGTH, required = true) Integer length) {
		return assessmentService.getFibonacciMethod(length);
	}

	@GetMapping(value = ConstantClass.URI_FACTORIAL)
	public Integer getFactorialMethod(@RequestParam(value = ConstantClass.NUMBER, required = true) Integer number) {
		return assessmentService.getFactorialMethod(number);
	}

}