package com.assessment.docker.Implementation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.assessment.docker.service.AssessmentService;

@Service
public class AssessmentServiceImplementation implements AssessmentService {

	public List<Integer> getFibonacciMethod(Integer length) {
		return calculateFibonacci(length);
	}

	private List<Integer> calculateFibonacci(Integer length) {
		List<Integer> list = new ArrayList<Integer>();
		int previousNumber = 0;
		int nextNumber = 1;
		for (int i = 1; i <= length; ++i) {
			list.add(previousNumber);
			int sum = previousNumber + nextNumber;
			previousNumber = nextNumber;
			nextNumber = sum;
		}
		return list;
	}

	public Integer getFactorialMethod(Integer number) {
		return calculateFactorial(number);
	}

	private int calculateFactorial(Integer number) {
		int i, factorial = 1;
		for (i = 1; i <= number; i++) {
			factorial = factorial * i;
		}
		return factorial;
	}

}