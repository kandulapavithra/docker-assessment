package com.assessment.docker.service;

import java.util.List;

public interface AssessmentService {

	List<Integer> getFibonacciMethod(Integer length);

	Integer getFactorialMethod(Integer number);

}
