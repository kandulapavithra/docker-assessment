package com.assessment.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDockerAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDockerAssessmentApplication.class, args);
	}

}
